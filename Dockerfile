# ---- Base Node ----
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN git clone https://github.com/bottlecaps-foundation/bottlecaps.git /opt/bottlecaps && \
    cd /opt/bottlecaps/src && \
    make -f makefile.unix

# ---- Release ----
FROM ubuntu:16.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev nano && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r bottlecaps && useradd -r -m -g bottlecaps bottlecaps
RUN mkdir /data
RUN chown bottlecaps:bottlecaps /data
COPY --from=build /opt/bottlecaps/src/bottlecapsd /usr/local/bin/
USER bottlecaps
VOLUME /data
EXPOSE 7685 8385
CMD ["/usr/local/bin/bottlecapsd", "-datadir=/data", "-conf=/data/BottleCaps.conf", "-server", "-txindex", "-printtoconsole"]